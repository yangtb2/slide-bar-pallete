﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            SliderBarExt.SlideBarExt.SlideValue slideValue1 = new SliderBarExt.SlideBarExt.SlideValue();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            SliderBarExt.SlideBarExt.SlideValue slideValue2 = new SliderBarExt.SlideBarExt.SlideValue();
            SliderBarExt.SlideBarExt.SlideValue slideValue3 = new SliderBarExt.SlideBarExt.SlideValue();
            this.slideBarExt1 = new SliderBarExt.SlideBarExt();
            this.SuspendLayout();
            // 
            // slideBarExt1
            // 
            this.slideBarExt1.BackdropRadius = false;
            this.slideBarExt1.BackdropThickness = 12;
            this.slideBarExt1.CursorWidth = 0;
            slideValue1.RectF = ((System.Drawing.RectangleF)(resources.GetObject("slideValue1.RectF")));
            slideValue1.SlideColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            slideValue1.Slidepoint = ((System.Drawing.PointF)(resources.GetObject("slideValue1.Slidepoint")));
            slideValue1.Tippoint = ((System.Drawing.PointF)(resources.GetObject("slideValue1.Tippoint")));
            slideValue1.TipRect = new System.Drawing.Rectangle(603, 501, 44, 18);
            slideValue1.Value = 0F;
            slideValue2.RectF = ((System.Drawing.RectangleF)(resources.GetObject("slideValue2.RectF")));
            slideValue2.SlideColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            slideValue2.Slidepoint = ((System.Drawing.PointF)(resources.GetObject("slideValue2.Slidepoint")));
            slideValue2.Tippoint = ((System.Drawing.PointF)(resources.GetObject("slideValue2.Tippoint")));
            slideValue2.TipRect = new System.Drawing.Rectangle(603, 477, 44, 18);
            slideValue2.Value = 0F;
            slideValue3.RectF = ((System.Drawing.RectangleF)(resources.GetObject("slideValue3.RectF")));
            slideValue3.SlideColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            slideValue3.Slidepoint = ((System.Drawing.PointF)(resources.GetObject("slideValue3.Slidepoint")));
            slideValue3.Tippoint = ((System.Drawing.PointF)(resources.GetObject("slideValue3.Tippoint")));
            slideValue3.TipRect = new System.Drawing.Rectangle(603, 453, 44, 18);
            slideValue3.Value = 0F;
            this.slideBarExt1.Items.Add(slideValue1);
            this.slideBarExt1.Items.Add(slideValue2);
            this.slideBarExt1.Items.Add(slideValue3);
            this.slideBarExt1.Location = new System.Drawing.Point(12, 32);
            this.slideBarExt1.Name = "slideBarExt1";
            this.slideBarExt1.Orientation = SliderBarExt.SlideBarExt.SlideOrientation.VerticalRight;
            this.slideBarExt1.Size = new System.Drawing.Size(687, 525);
            this.slideBarExt1.SlideHeight = 3;
            this.slideBarExt1.SlideProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.slideBarExt1.SlideRadius = 15;
            this.slideBarExt1.SlideWidth = 25;
            this.slideBarExt1.TabIndex = 0;
            this.slideBarExt1.Text = "slideBarExt1";
            this.slideBarExt1.TipBackdropColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.slideBarExt1.TipColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.slideBarExt1.TipFont = new System.Drawing.Font("宋体", 13F);
            this.slideBarExt1.TipShow = SliderBarExt.SlideBarExt.TipShowEnum.Auto;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 590);
            this.Controls.Add(this.slideBarExt1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SliderBarExt.SlideBarExt slideBarExt1;
    }
}


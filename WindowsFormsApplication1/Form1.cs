﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            slideBarExt1.CanRemoveSlide = true; //控件默认false
            slideBarExt1.EnableColorChange = true; //控件默认true
            slideBarExt1.Items[0].Value = 0;
            slideBarExt1.Items[1].Value = 50;
            slideBarExt1.Items[2].Value = 100;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Design;
using System.Reflection;

namespace System.ComponentModel.Design
{
  /// <summary>
  /// 集合属性编辑窗口扩展(显示帮助文本)
  /// </summary>

  [Description("集合属性编辑窗口扩展(显示帮助文本)")]
  public class CollectionEditorExt : CollectionEditor
  {
    public CollectionEditorExt(Type type)
      : base(type)
    {
    }

    protected override CollectionForm CreateCollectionForm()
    {
      CollectionForm frm = base.CreateCollectionForm();
      FieldInfo fileinfo = frm.GetType().GetField("propertyBrowser", BindingFlags.NonPublic | BindingFlags.Instance);
      if (fileinfo != null)
      {
        (fileinfo.GetValue(frm) as System.Windows.Forms.PropertyGrid).HelpVisible = true;//显示帮助文本
      }
      frm.Width = 400;
      frm.Height = 400;
      return frm;
    }
  }
}
